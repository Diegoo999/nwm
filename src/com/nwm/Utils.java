package com.nwm;

public class Utils {

	public static String replaceWhenEmpty(String k, String r) {
		if (r != null && !r.equalsIgnoreCase("")) {
			k = r;
		}
		return k;
	}
	
	public static Object replaceWhenEmpty(Object k, Object r) {
		if (r != null) {
			k = r;
		}
		return k;
	}
}
