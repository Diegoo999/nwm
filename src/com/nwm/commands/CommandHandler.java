package com.nwm.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.nwm.commands.systemcommands.HelpCommand;
import com.nwm.commands.systemcommands.ServerCommand;
import com.nwm.commands.systemcommands.StopCommand;

public class CommandHandler extends Thread {

	private static List<Command> commands = new ArrayList<Command>();

	private BufferedReader is;
	private static Redirector redirector = null;

	public static void init() {

		// Register system commands
		registerSystemCommands();

		// Start listening to commands
		try {
			CommandHandler.class.newInstance().start();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/* Setters */

	public static void setRedirector(Redirector r) {
		redirector = r;
	}

	public static void removeRedirector() {
		redirector = null;
	}

	/* Methods */

	public static void registerCommand(Command cmd) {
		commands.add(cmd);
	}

	public static void unregisterCommand(Command cmd) {
		commands.remove(cmd);
	}

	public static Command getCommand(String label) {
		for (Command cmd : commands) {
			if (cmd.getName().equalsIgnoreCase(label)) {
				return cmd;
			}
			if (cmd.getAliases() != null) {
				for (String s : cmd.getAliases()) {
					if (s.equalsIgnoreCase(label)) {
						return cmd;
					}
				}
			}
		}
		return null;
	}

	public static List<Command> getCommands() {
		return commands;
	}

	/* Other */

	private static void registerSystemCommands() {
		new Command("stop", new String[] { "halt" }, "Stop the server", new CommandExecutor[] { new StopCommand() }, true);
		new Command("help", new String[] { "?" }, "Display info about commands", new CommandExecutor[] { new HelpCommand() }, true);
		new Command("server", null, "Manage servers from your network.", new CommandExecutor[] { new ServerCommand() }, true);
	}

	@Override
	public void run() {
		is = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try {
			while ((line = is.readLine()) != null) {
				if (redirector == null) {
					String label = line.split(" ")[0];
					Command cmd = getCommand(label);
					String args[] = new String[0];
					if (line.length() > label.length() + 1) {
						args = line.substring(label.length() + 1).split(" ");
					}

					if (cmd != null) {
						cmd.execute(null, label, args);
					} else {
						System.out.println("Unknown command! Please use 'help' for more information and commands.");
					}
				} else {
					if (line.equalsIgnoreCase("!exit")) {
						redirector.stopRedirecting();
					} else {
						redirector.getOutputStream().println(line);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
