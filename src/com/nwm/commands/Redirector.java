package com.nwm.commands;

import java.io.PrintWriter;

public interface Redirector {

	public PrintWriter getOutputStream();

	public void stopRedirecting();

	public void startRedirecting();

}
