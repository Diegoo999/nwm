package com.nwm.commands;

public interface CommandExecutor {

	public void onCommand(Sender sender, Command command, String label, String[] args);

}
