package com.nwm.commands;

public class Command {

	private String name;
	private CommandExecutor[] executors;
	private String[] aliases = null;
	private String description = null;

	public Command(String name, CommandExecutor[] executors) {
		super();
		this.name = name;
		this.executors = executors;
	}

	public Command(String name, String[] aliases, String desc, CommandExecutor[] executors, boolean register) {
		this.name = name;
		this.aliases = aliases;
		this.description = desc;
		this.executors = executors;
		if (register) {
			CommandHandler.registerCommand(this);
		}
	}

	/* Getters */

	public String getName() {
		return name;
	}

	public CommandExecutor[] getExecutors() {
		return executors;
	}

	public String[] getAliases() {
		return aliases;
	}

	public String getDescription() {
		return description;
	}

	/* Setters */

	public void setAliases(String[] aliases) {
		this.aliases = aliases;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}

	/* Methods */

	public void execute(Sender sender, String label, String[] args) {
		for (CommandExecutor ex : executors) {
			ex.onCommand(sender, this, label, args);
		}
	}
}
