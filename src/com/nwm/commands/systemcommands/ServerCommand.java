package com.nwm.commands.systemcommands;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.nwm.Main;
import com.nwm.commands.Command;
import com.nwm.commands.CommandExecutor;
import com.nwm.commands.Sender;
import com.nwm.servers.Server;
import com.nwm.servers.ServerHandler;

public class ServerCommand implements CommandExecutor {

	@Override
	public void onCommand(Sender sender, Command command, String label, String[] args) {
		if (command.getName().equalsIgnoreCase("server")) {
			if (args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("help"))) {
				System.out.println("Help command of server:\nserver list - Display a list of running servers.\nserver create <config> <name> - Create a new server.\nserver delete <name> - Delete a server.\nserver start <name> - Start a specified server.\nserver stop <name> - Stop a specified server.\nserver control <name> - Open the server terminal.");
				return;
			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("list")) {
					StringBuilder sb = new StringBuilder("Running servers:");
					if (ServerHandler.getServers().size() > 0) {
						for (Server server : ServerHandler.getServers()) {
							sb.append("\n- " + server.getServerName());
						}
					} else {
						sb.append("\n- none");
					}
					System.out.println(sb.toString());
					return;
				}
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("stop")) {
					Server server = ServerHandler.getServer(args[1]);
					if (server != null) {
						server.stopServer();
					} else {
						System.out.println("This server does not exists or is not running!");
					}
					return;
				} else if (args[0].equalsIgnoreCase("start")) {
					Server server = new Server(args[1]);
					if (server.getServerWorkspace().exists()) {
						server.startServer();
						System.out.println("Server is starting. Please use 'server control " + args[1] + "' to control the server.");
					} else {
						System.out.println("This server does not exists!");
					}
					return;
				} else if (args[0].equalsIgnoreCase("delete")) {
					Server server = ServerHandler.getServer(args[1]);
					if (server == null) {
						for (File f : new File(Main.getWorkspace(), "servers").listFiles()) {
							if (f.isDirectory() && f.getName().equalsIgnoreCase(args[1])) {
								try {
									FileUtils.deleteDirectory(f);
									System.out.println("Server has been deleted!");
								} catch (IOException e) {
									System.out.println("Error while deleting server!");
								}
								return;
							}
						}
						System.out.println("The server does not exist!");
					} else {
						System.out.println("The server is running! Please close the server before deleting it.");
					}
					return;
				} else if (args[0].equalsIgnoreCase("control")) {
					for (Server server : ServerHandler.getServers()) {
						if (server.getServerName().equalsIgnoreCase(args[1]) && server.isRunning()) {
							server.startRedirecting();
							return;
						}
					}
					System.out.println("This server is not running or does not exists!");
					return;
				}
			} else if (args.length == 3) {
				if (args[0].equalsIgnoreCase("create")) {
					if (!ServerHandler.serverExists(args[2])) {
						File f = new File(Main.getWorkspace(), "resources/configurations");
						for (File t : f.listFiles()) {
							if (t.isFile() && t.getName().equalsIgnoreCase(args[1] + ".properties")) {
								System.out.println("Creating server...");
								new Server(args[2], t);
								System.out.println("Done! Please use 'server start " + args[2] + "' to start the server.");
								return;
							}
						}
						System.out.println("That config does not exists!");
					} else {
						System.out.println("This server name is already taken!");
						return;
					}
					System.out.println("Server configuration does not exists!");
					return;
				}
			}
			System.out.println("Invalid syntax! Please use 'server help' for more information and commands.");
		}
	}
}
