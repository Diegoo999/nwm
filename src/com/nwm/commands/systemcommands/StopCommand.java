package com.nwm.commands.systemcommands;

import com.nwm.Main;
import com.nwm.commands.Command;
import com.nwm.commands.CommandExecutor;
import com.nwm.commands.Sender;

public class StopCommand implements CommandExecutor {

	@Override
	public void onCommand(Sender sender, Command command, String label, String[] args) {

		if (command.getName().equalsIgnoreCase("stop")) {
			System.out.println("Stopping server...");
			Main.onDisable();
			System.exit(0);
		}
	}

}
