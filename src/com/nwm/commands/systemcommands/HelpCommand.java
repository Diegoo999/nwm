package com.nwm.commands.systemcommands;

import com.nwm.commands.Command;
import com.nwm.commands.CommandExecutor;
import com.nwm.commands.CommandHandler;
import com.nwm.commands.Sender;

public class HelpCommand implements CommandExecutor {

	@Override
	public void onCommand(Sender sender, Command command, String label, String[] args) {

		if (command.getName().equalsIgnoreCase("help")) {
			if (args.length == 0) {
				System.out.println("Command information:\nhelp list - Display a list of registered commands.\nhelp info <command> - Display information about a specific comamnd.");
				return;
			} else if (args.length == 1) {
				if (args[0].equalsIgnoreCase("list")) {
					StringBuilder sb = new StringBuilder("List of registered commands:");
					for (Command cmd : CommandHandler.getCommands()) {
						String desc = "no description";
						if (cmd.getDescription() != null) {
							desc = cmd.getDescription();
						}
						sb.append("\n" + cmd.getName() + " - " + desc);
					}
					System.out.println(sb);
					return;
				} else if (args[0].equalsIgnoreCase("info")) {
					System.out.println("Invalid syntax! Please use 'help info <command>'.");
					return;
				}
			} else if (args.length == 2) {
				if (args[0].equalsIgnoreCase("info")) {
					Command cmd = CommandHandler.getCommand(args[1]);
					if (cmd != null) {
						System.out.println("Command information from: " + cmd.getName() + "\nDescription: " + cmd.getDescription() + "\nAliases:");
						if (cmd.getAliases() != null) {
							for (String s : cmd.getAliases()) {
								System.out.print("- " + s);
							}
						} else {
							System.out.print("- none");
						}
					} else {
						System.out.println("Command not found!");
					}
					return;
				}
			}
			System.out.println("Invalid syntax! Please use 'help' for more information.");
		}
	}

}
