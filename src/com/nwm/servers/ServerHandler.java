package com.nwm.servers;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.nwm.Main;
import com.nwm.files.FilesHandler;

public class ServerHandler {

	private static List<Server> running = new ArrayList<Server>();
	public static List<Integer> ports = new ArrayList<Integer>();

	private static int ports_start = 25565;
	private static int ports_end = 26000;

	public static void setupServers() {
		String[] parts = FilesHandler.getConfig().getString("port-range").split("-");
		try {
			ports_start = Integer.parseInt(parts[0]);
			ports_end = Integer.parseInt(parts[1]);
		} catch (NumberFormatException e) {
			System.err.println("Error in config while parsing port range!");
		}
	}

	public static Collection<Server> getServers() {
		return running;
	}

	public static Server getServer(String name) {
		for (Server server : running) {
			if (server.getServerName().equalsIgnoreCase(name)) {
				return server;
			}
		}
		return null;
	}

	public static void registerServer(Server server) {
		running.add(server);
	}

	public static void unregisterServer(Server server) {
		running.remove(server);
	}

	public static void unregisterPort(int port) {
		for (int i = 0; i < ServerHandler.ports.size(); i++) {
			int p = ServerHandler.ports.get(i);
			if (port == p) {
				ServerHandler.ports.remove(i);
			}
		}
	}

	public static boolean serverExists(String name) {
		for (File f : new File(Main.getWorkspace(), "servers").listFiles()) {
			if (f.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	public static boolean configExists(String name) {
		File f = new File(Main.getWorkspace(), "resources/configurations");
		for (File t : f.listFiles()) {
			if (t.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}

	public static int getAvailiblePort() {
		for (int i = ports_start; i < ports_end; i++) {
			if (!ports.contains(i)) {
				try {
					(new Socket("127.0.0.1", i)).close();
				} catch (IOException e) {
					return i;
				}
			}
		}
		return 0;
	}
}
