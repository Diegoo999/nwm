package com.nwm.servers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.apache.commons.io.FileUtils;

import com.nwm.Main;
import com.nwm.Utils;
import com.nwm.commands.CommandHandler;
import com.nwm.commands.Redirector;
import com.nwm.files.FileConfiguration;

public class Server extends Thread implements Redirector {

	private File dir;
	private File executable = new File(Main.getWorkspace(), "resources/executable/server.jar");
	private String ram = "-Xmx1G";
	private String host = "localhost";
	private int port = -1;

	private boolean online_mode = true;
	private int max_players = 35;

	private Process process;
	public BufferedReader is;
	public PrintWriter os;

	private boolean logging = false;

	/**
	 * Create a server from a config.
	 */
	public Server(String name, File config) {
		super();
		this.dir = new File(Main.getWorkspace(), "servers/" + name);

		FileConfiguration conf = new FileConfiguration(config);

		if (conf.getString("executable") != null && !conf.getString("executable").equalsIgnoreCase("")) {
			executable = new File(Main.getWorkspace(), "resources/executables/" + conf.getString("executable"));
			if (executable != null) {
				File dest = new File(dir, conf.getString("executable"));
				try {
					FileUtils.copyFile(executable, dest);
					this.executable = dest;
				} catch (IOException e) {
					dir.delete();
					System.out.println("Error while copying executable!");
					return;
				}
			} else {
				dir.delete();
				System.out.println("Error while creating server. Executable not found!");
				return;
			}
		} else {
			dir.delete();
			System.out.println("Error while creating server. Executable not found!");
			return;
		}

		if (conf.getString("port") != null && !conf.getString("port").equalsIgnoreCase("")) {
			if (!conf.getString("port").equalsIgnoreCase("dynamic")) {
				try {
					port = Integer.parseInt(conf.getString("port"));
				} catch (NumberFormatException e) {
					System.out.println("Invalid port number in the config. Port is now dynamic choosen.");
				}
			}
		}

		if (conf.getString("maps") != null && !conf.getString("maps").equalsIgnoreCase("")) {
			String[] parts = conf.getString("maps").split(",");
			if (parts != null && parts.length > 0) {
				a: for (String s : parts) {
					String[] maps = s.split(":");
					if (!maps[1].equalsIgnoreCase("new")) {
						for (File f : new File(Main.getWorkspace(), "resources/maps").listFiles()) {
							if (f.isDirectory() && f.getName().equalsIgnoreCase(maps[1])) {
								File dest = new File(dir, maps[0]);
								try {
									FileUtils.copyDirectory(f, dest);
								} catch (IOException e) {
									System.out.println("Error while copying map to " + dest.getName() + "!");
								}
								continue a;
							}
						}
						System.out.println("The map '" + maps[1] + "' does not exists! Skipping this map...");
					}
				}
			} else {
				System.out.println("Invalid map syntax in config!");
			}
		}

		if (conf.getString("plugins") != null && !conf.getString("plugins").equalsIgnoreCase("")) {
			String[] pls = conf.getString("plugins").split(",");
			File dest = new File(Main.getWorkspace(), "resources/plugins");

			a: for (String pl : pls) {
				for (File f : dest.listFiles()) {
					if (f.isFile() && f.getName().equalsIgnoreCase(pl + ".jar")) {
						try {
							FileUtils.copyFile(f, new File(dir, "plugins/" + f.getName()));
						} catch (IOException e) {
							System.out.println("Error while copying '" + pl + "' plugin!");
						}
						continue a;
					}
				}
				System.out.println("The plugin '" + pl + "' does not exists! Skipping this plugin...");
			}
		}

		ram = Utils.replaceWhenEmpty(ram, conf.getString("ram"));
		host = Utils.replaceWhenEmpty(host, conf.getString("host"));
		max_players = (int) Utils.replaceWhenEmpty(max_players, conf.getInteger("max-players"));
		online_mode = (boolean) Utils.replaceWhenEmpty(online_mode, conf.getBoolean("online-mode"));

		FileConfiguration nwm = new FileConfiguration(new File(dir, "nwm.properties"));
		nwm.setProperty("host", host);
		nwm.setProperty("ram", ram);
		nwm.setProperty("executable", executable.getName());
		if (port == -1) {
			nwm.setProperty("port", "dynamic");
		} else {
			nwm.setProperty("port", port);
		}
		nwm.setProperty("online-mode", online_mode);
		nwm.setProperty("max-players", max_players);
		nwm.saveConfig();
		acceptEula();
	}

	/**
	 * Load a existing server.
	 */
	public Server(String name) {
		super();
		this.dir = new File(Main.getWorkspace(), "servers/" + name);

		if (dir.exists()) {
			FileConfiguration config = new FileConfiguration(new File(dir, "nwm.properties"));
			if (config.getString("port") != null && !config.getString("port").equalsIgnoreCase("")) {
				if (!config.getString("port").equalsIgnoreCase("dynamic")) {
					try {
						this.port = Integer.parseInt(config.getString("port"));
					} catch (NumberFormatException e) {
						System.out.println("Invalid port number in the NWM config. Port is now dynamic choosen.");
					}
				}
			}

			executable = new File(dir, config.getString("executable"));
			host = config.getString("host");
			ram = config.getString("ram");
			online_mode = config.getBoolean("online_mode");
			max_players = config.getInteger("max-players");
		}
	}

	/* Getters */

	public boolean isRunning() {
		if (process.isAlive()) {
			return true;
		}
		return false;
	}

	public String getServerName() {
		return dir.getName();
	}

	public File getServerWorkspace() {
		return dir;
	}

	/* Methods */

	@SuppressWarnings("deprecation")
	public void deleteServer() {
		if (process.isAlive()) {
			process.destroyForcibly();
		}
		if (this.isAlive()) {
			this.stop();
		}
		dir.delete();
	}

	private void acceptEula() {
		FileWriter fw;
		try {
			fw = new FileWriter(new File(dir, "eula.txt"));
			fw.append("eula=true");
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void startServer() {
		if (port == -1) {
			port = ServerHandler.getAvailiblePort();
			ServerHandler.ports.add(port);
		}
		if (process == null || !process.isAlive()) {
			ProcessBuilder pb = new ProcessBuilder("java", ram, "-jar", executable.getAbsolutePath(), "--nojline", "--log-strip-color", "--port", port + "", "--host", host, "--size", max_players + "", "--online-mode", online_mode + "");
			pb.directory(dir);
			try {
				process = pb.start();
				is = new BufferedReader(new InputStreamReader(process.getInputStream()));
				os = new PrintWriter(process.getOutputStream(), true);
				ServerHandler.registerServer(this);
				this.start();
				System.out.println("Starting server on " + host + ":" + port + "...");
			} catch (IOException e) {
				System.err.println("An error eccoured while starting '" + getServerName() + "'!");
			}
		}
	}

	public void stopServer() {
		System.out.println("Stopping server...");
		sendCommand("stop");
	}

	public void sendCommand(String cmd) {
		os.println(cmd);
	}

	/* Thread */

	@Override
	public void run() {
		String line = null;
		try {
			while ((line = is.readLine()) != null) {
				if (logging) {
					System.out.println("[" + getServerName() + "]: " + line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("The server '" + getServerName() + "' is now down.");
		ServerHandler.unregisterServer(this);
		ServerHandler.unregisterPort(port);
		stopRedirecting();
	}

	/* Redirector */

	@Override
	public void startRedirecting() {
		CommandHandler.setRedirector(this);
		logging = true;
	}

	@Override
	public PrintWriter getOutputStream() {
		return os;
	}

	@Override
	public void stopRedirecting() {
		logging = false;
		CommandHandler.removeRedirector();
	}
}
