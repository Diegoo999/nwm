package com.nwm;

import java.io.File;

import com.nwm.commands.CommandHandler;
import com.nwm.files.FilesHandler;
import com.nwm.servers.Server;
import com.nwm.servers.ServerHandler;

public class Main {

	public static void main(String[] args) {
		long c = System.currentTimeMillis();
		System.out.println("Loading modules...");

		// Generate and init files
		FilesHandler.init();

		// Init commands
		CommandHandler.init();

		System.out.println("Loading done! Took " + (System.currentTimeMillis() - c) + "ms. Type 'help' for more information and commands.");
	}

	public static void onDisable() {
		for (Server server : ServerHandler.getServers()) {
			server.stopServer();
		}
	}

	/** Get workspace location **/
	public static File getWorkspace() {
		String path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.substring(0, path.lastIndexOf("/"));
		path = path.replaceAll("%20", " ");
		return new File(path);
	}
}
