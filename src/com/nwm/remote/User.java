package com.nwm.remote;

public class User extends Thread {

	private String username;
	private boolean authed = false;

	private Session session;

	public User(Session session) {
		super();
		this.start();
	}

	/* Getters */

	public boolean isAuthed() {
		return authed;
	}

	public String getUsername() {
		return username;
	}

	/* Methods */

	public boolean auth(String session) {
		
		//TODO: If authed register to SessionHandler.sessions
		//TODO: Disconnect other connected users
		
		return false;
	}

	/* Thread */

	@Override
	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (!authed && !session.isClosed()) {
			session.sendMessage("Timed out!");
			session.close();
		}
	}
}
