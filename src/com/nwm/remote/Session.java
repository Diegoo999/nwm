package com.nwm.remote;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Session extends Thread {

	private Socket socket;
	private BufferedReader is;
	public PrintWriter os;

	private User user;

	public Session(Socket socket) {
		super();
		this.socket = socket;
		this.start();
		user = new User(this);
	}

	/* Getters */

	public boolean isClosed() {
		return socket.isClosed();
	}

	/* Methods */

	public void sendMessage(String msg) {
		os.println(msg);
		os.flush();
	}

	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			os = new PrintWriter(socket.getOutputStream(), true);

			String line = null;
			while ((line = is.readLine()) != null) {
				if (!user.isAuthed()) {
					os.println(line);
				} else {
					user.auth(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
