package com.nwm.remote;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import com.nwm.files.FilesHandler;

public class SessionHandler extends Thread {

	private static HashMap<String, User> sessions = new HashMap<String, User>();

	private static ServerSocket serversocket;
	private static int port = 23440;

	public static void init() {
		port = FilesHandler.getConfig().getInteger("remote-port");
		try {
			serversocket = new ServerSocket(port);
			SessionHandler.class.newInstance().start();
		} catch (IOException | InstantiationException | IllegalAccessException e) {
			System.out.println("Error while starting remote server!");
		}

		// TODO: Clear all sessions

	}

	/* Methods */

	public static void registerSession(User user) {

	}

	public static void unregisterSession(User user) {

	}

	/* Thread */

	@Override
	public void run() {
		System.out.println("Starting remote server on " + serversocket.getInetAddress().getHostAddress() + ":" + port);
		Socket socket = null;
		try {
			while ((socket = serversocket.accept()) != null) {
				new Session(socket);
				System.out.println("New connection from " + socket.getInetAddress().getHostAddress() + "!");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
