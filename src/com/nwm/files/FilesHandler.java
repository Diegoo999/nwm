package com.nwm.files;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.nwm.Main;

public class FilesHandler {

	private static String[] dirs = new String[] { "resources", "resources/executables", "resources/maps", "resources/plugins", "resources/configurations", "logs", "servers", "plugins" };
	private static String[] files = new String[] { "server.properties"};

	private static FileConfiguration config = null;

	public static void init() {

		// Generate files
		for (String s : dirs) {
			new File(Main.getWorkspace(), s).mkdir();
		}
		for (String s : files) {
			try {
				new File(Main.getWorkspace(), s).createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Load config
		HashMap<String, Object> defaults = new HashMap<String, Object>();
		defaults.put("port-range", "25565-26000");
		defaults.put("remote-port", "23440");
		config = new FileConfiguration(new File(Main.getWorkspace(), "server.properties"));
	}

	public static FileConfiguration getConfig() {
		return config;
	}
}
