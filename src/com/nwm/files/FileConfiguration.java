package com.nwm.files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import com.nwm.Main;

public class FileConfiguration {

	private File file;
	private LinkedHashMap<String, Object> props = new LinkedHashMap<String, Object>();
	private HashMap<String, Object> defaults = null;

	public FileConfiguration(File file) {
		super();
		this.file = file;
		loadToMemory();
	}

	public FileConfiguration(String path) {
		super();
		this.file = new File(Main.getWorkspace(), path);
		loadToMemory();
	}

	@SuppressWarnings("resource")
	private void loadToMemory() {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			int i = 0;
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				i++;
				line = line.trim();
				if (line.startsWith("#")) {
					continue;
				}
				if (!line.contains("=")) {
					System.err.println("Invalid property at line " + i + " in " + file.getAbsolutePath() + "!");
					continue;
				}
				props.put(line.substring(0, line.indexOf("=")).trim(), line.substring(line.indexOf("=") + 1).trim());
			}
		} catch (IOException e) {
		}
		if (defaults != null) {
			addDefaults();
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a string.
	 **/
	public String getString(String key) {
		return props.get(key) + "";
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a boolean.
	 **/
	public boolean getBoolean(String key) {
		try {
			return Boolean.parseBoolean((String) props.get(key));
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a integer.
	 **/
	public int getInteger(String key) {
		try {
			return Integer.parseInt((String) props.get(key));
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a long.
	 **/
	public long getLong(String key) {
		try {
			return Long.parseLong((String) props.get(key));
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a double.
	 **/
	public double getDouble(String key) {
		try {
			return Double.parseDouble((String) props.get(key));
		} catch (Exception e) {
			return 0.0;
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a float.
	 **/
	public float getFloat(String key) {
		try {
			return Float.parseFloat((String) props.get(key));
		} catch (Exception e) {
			return 0f;
		}
	}

	/**
	 * Get the value of a key that is stored in memory.
	 * 
	 * @return the value of the key as a byte.
	 **/
	public byte getByte(String key) {
		try {
			return Byte.parseByte((String) props.get(key));
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * Get all loaded keys from the configuration.
	 * 
	 * @return a list of keys.
	 */
	public Set<String> getKeys() {
		return props.keySet();
	}

	/**
	 * Check if the properties contains a specific key.
	 */
	public boolean containsKey(String key) {
		if (props.containsKey(key)) {
			return true;
		}
		return false;
	}

	/**
	 * Check if the properties contains a specific value.
	 */
	public boolean containsValue(String value) {
		if (props.containsValue(value)) {
			return true;
		}
		return false;
	}

	/**
	 * Change a value or add a new key and value to the configuration.
	 */
	public void setProperty(String key, Object value) {
		props.put(key, value.toString());
	}

	/**
	 * Save the loaded configuration data to disk.
	 */
	public void saveConfig() {
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(file));
			StringBuilder sb = new StringBuilder();
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				line.trim();
				if (line.startsWith("#")) {
					sb.append(line + "\n");
					continue;
				}
				String[] kv = new String[] { line.substring(0, line.indexOf("=")).trim(), line.substring(line.indexOf("=") + 1).trim() };
				if (props.containsKey(kv[0])) {
					sb.append(kv[0] + "=" + props.get(kv[0]) + "\n");
					props.remove(kv[0]);
					continue;
				}
				sb.append(kv[0] + "=" + kv[1] + "\n");
			}
			for (String s : props.keySet()) {
				sb.append(s + "=" + props.get(s) + "\n");
			}
			FileWriter fw = new FileWriter(file);
			fw.write(sb.toString());
			fw.close();
			props.clear();
			loadToMemory();
		} catch (IOException e) {
		}
	}

	/**
	 * Reload all data from disk to memory.
	 * 
	 * @warning all data what is not saved to the disk will be overwritten!
	 */
	public void reloadConfig() {
		props.clear();
		loadToMemory();
		if (defaults != null) {
			addDefaults();
		}
	}

	/**
	 * Dump all configuration keys and values to the console.
	 */
	public void dump() {
		StringBuilder sb = new StringBuilder("Loaded properties=");
		int i = 1;
		for (String s : props.keySet()) {
			sb.append("\n" + i + " > " + s + "=" + props.get(s));
			i++;
		}
		System.out.println(sb.toString());
	}

	public void setDefaults(HashMap<String, Object> defaults) {
		this.defaults = defaults;
		addDefaults();
	}

	private void addDefaults() {
		for (String s : defaults.keySet()) {
			if (!props.containsKey(s)) {
				props.put(s, defaults.get(s));
			}
		}
	}
}
